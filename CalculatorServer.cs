using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;

namespace server
{
    class Program
    {
        private static void ProcessClientRequests(object argument)
        {
            TcpClient client = (TcpClient)argument;
            try {

			StreamReader streamReader = new StreamReader(client.GetStream());
			StreamWriter streamWriter = new StreamWriter(client.GetStream());

            string messageString = " ";
            int input, n1, n2, sum;
           
            		while (true){
            			input = int.Parse(streamReader.ReadLine());;
            			if (input == 5)
      	 					break;
            			
            			Console.WriteLine("Klien Input : " + input);
            			
            			n1 = int.Parse(streamReader.ReadLine());
            			n2 = int.Parse(streamReader.ReadLine());
            			
            			switch (input) {
   					  	  case 1:
    			      	  sum = n1 + n2;
    			      	  Console.WriteLine("Penjumlahan : " + n1 + " + " + n2 + " = " + sum);
    			      	  Console.WriteLine();
   					  	  break;
   					  	  case 2:
   					  	  sum = n1 - n2;
   					  	  Console.WriteLine("Pengurangan : " + n1 + " - " + n2 + " = " + sum);
   					  	  Console.WriteLine();
      			          break;
      			          case 3:
   					  	  sum = n1 * n2;
   					  	  Console.WriteLine("Perkalian : " + n1 + " x " + n2 + " = " + sum);
   					  	  Console.WriteLine();
      			          break;
      			          case 4:
   					  	  sum = n1 / n2;
   					  	  Console.WriteLine("Pembagian : " + n1 + " / " + n2 + " = " + sum);
   					  	  Console.WriteLine();
      			          break;
      			          default:
      			          sum = 0;
      			          break;
      				   }

            			messageString = sum.ToString();
            			streamWriter.WriteLine(messageString); 
            			streamWriter.Flush();
 		 		 	}

            streamReader.Close();
			streamWriter.Close();
			client.Close();
			Console.WriteLine("Koneksi ke salah satu klien terputus!");
			Console.WriteLine("");
            }
            catch(IOException){
                Console.WriteLine("Komunikasi klien bermasalah. Menutup thread.");
                } 
            finally{
			if(client != null){
                client.Close();
                }
            }
        }
    

        public static void Main(){
		TcpListener listener = null;
		try {
			listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
			listener.Start();
			Console.WriteLine("Memulai multi-threading...");
			Console.WriteLine("");
			while(true){
				Console.WriteLine("Menunggu sambungan klien...");
				TcpClient client = listener.AcceptTcpClient();
				Console.WriteLine("Menerima klien baru...");
				Console.WriteLine("");
				Thread t = new Thread(ProcessClientRequests);
				t.Start(client);
			}
		} catch(Exception e){
				Console.WriteLine(e);
		} finally{
				if(listener != null){
					listener.Stop();
				}
		    }
        }
    }
}
