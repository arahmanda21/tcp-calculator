using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class EchoClient {
    public static void Main()
    {
        try {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            StreamReader streamReader = new StreamReader(client.GetStream());
            StreamWriter streamWriter = new StreamWriter(client.GetStream());
            String s = String.Empty;
            
            Console.WriteLine("Server telah terkoneksi!");
            while (true)
            {
           			// Baca pesan dari server dan tampilkan di console
            		string input, n1, n2, sum;

            		Console.WriteLine("Server - Kalkulator ");
            		Console.WriteLine("1. Penjumlahan ");
            		Console.WriteLine("2. Pengurangan ");
            		Console.WriteLine("3. Perkalian");
            		Console.WriteLine("4. Pembagian");
            		Console.WriteLine("5. Keluar");
            		Console.Write("Pilihan : ");
            		
		    		input = Console.ReadLine(); 
		    		Console.WriteLine();	
					
            		streamWriter.WriteLine(input); 
            		streamWriter.Flush();
            		
            		if (input == "5")
      	 					break;

            			Console.Write("Masukkan angka pertama : ");
            			n1 = Console.ReadLine();            
            			streamWriter.WriteLine(n1); 
            			streamWriter.Flush();
            			
            			Console.Write("Masukkan angka kedua : ");
            			n2 = Console.ReadLine();            
            			streamWriter.WriteLine(n2); 
            			streamWriter.Flush();
            			
            			sum = streamReader.ReadLine();
            			Console.WriteLine("Hasil : " + sum);
            			Console.WriteLine();
            }
        
        Console.WriteLine("Koneksi server terputus");
        Console.ReadLine();
        streamReader.Close();
        streamWriter.Close();
        client.Close();
        }
    	
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}
